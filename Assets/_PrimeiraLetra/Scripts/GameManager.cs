﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Answer
{
    public string Name;
    public Sprite Sprite;
}

public class GameManager : MonoBehaviour {

    [System.Serializable]
    public class GameEvent : UnityEvent { }

    public List<Answer> Answers;
    public ObjectDisplay Display;

    public int MaxNumberOfLetters = 5;
    public DragLetter LetterPrefab;
    public Transform LettersContainer;

    private List<DragLetter> _letters = new List<DragLetter>();

    public GameEvent OnGameStart = new GameEvent();
    public GameEvent OnGameEnd = new GameEvent();
    public GameEvent OnNewWord = new GameEvent();
    public GameEvent OnCorrect = new GameEvent();
    public GameEvent OnWrong = new GameEvent();

    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameManager>();

            return _instance;
        }
    }

    void Start()
    {
        StartGame();
    }

    public void StartGame()
    {
        RandomNewWord();

        OnGameStart.Invoke();
    }

    private void RandomNewWord()
    {
        int answerIndex = Random.Range(0, Answers.Count);
        NewWord(answerIndex);
    }

    private void NewWord(int answerIndex)
    {
        Answer answer = Answers[answerIndex];

        Display.SetAnswer(answer);
        CreateLetters(answer.Name);

        OnNewWord.Invoke();
    }

    public void CreateLetters(string answer)
    {
        string firstLetter = answer[0].ToString().ToUpper();

        List<string> availableLetters = new List<string>() { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        availableLetters.Remove(firstLetter);

        DragLetter letter;
        if (_letters.Count <= 0)
        {
            letter = Instantiate(LetterPrefab, LettersContainer);
            _letters.Add(letter);
        }
        else
        {
            letter = _letters[0];
        }
        letter.Value = firstLetter;

        for (int i = 1; i < MaxNumberOfLetters; i++)
        {
            if (_letters.Count <= i)
            {
                letter = Instantiate(LetterPrefab, LettersContainer);
                _letters.Add(letter);
            }
            else
            {
                letter = _letters[i];
            }

            int letterIndex = Random.Range(0, availableLetters.Count);
            letter.Value = availableLetters[letterIndex];
            availableLetters.RemoveAt(letterIndex);
        }

    }

    public bool CheckMatch(DragLetter letter, LetterSlot slot)
    {
        Debug.Log(letter.Value + "  " + slot.CorrectValue);

        bool correct = slot.IsAvailable && letter.Value == slot.CorrectValue;

        if (correct)
        {
            OnCorrect.Invoke();

            slot.Value = letter.Value;
            RandomNewWord();
        }
        else
        {
            OnWrong.Invoke();
        }

        return correct;
    }
}
