﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterSlot : MonoBehaviour {

    public Text Text;
    public Image Frame;
    public string CorrectValue;

    public string Value
    {
        get
        {
            return Text.text;
        }
        set
        {
            Text.text = value;
            Frame.gameObject.SetActive(IsAvailable);
        }
    }

    public bool IsAvailable
    {
        get
        {
            return string.IsNullOrEmpty(Value);
        }
    }
}
