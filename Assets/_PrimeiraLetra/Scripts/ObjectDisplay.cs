﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectDisplay : MonoBehaviour {

    public Image Image;
    public string Answer;
    public LetterSlot SlotPrefab;
    public Transform SlotsContainer;

    private List<LetterSlot> _slots = new List<LetterSlot>();

    public void CreateSlots()
    {
        ClearSlots();

        /*for (int i = 0; i < Answer.Length; i++)
        {
            LetterSlot slot = Instantiate(SlotPrefab, SlotsContainer);
            slot.CorrectValue = Answer[i].ToString();
            if (i != 0)
                slot.Value = Answer[i].ToString();

            _slots.Add(slot);
        }*/

        LetterSlot slot = Instantiate(SlotPrefab, SlotsContainer);
        slot.CorrectValue = Answer[0].ToString();
        _slots.Add(slot);
    }

    public void ClearSlots()
    {
        for (int i = 0; i < _slots.Count; i++)
        {
            Destroy(_slots[i].gameObject);
        }

        _slots = new List<LetterSlot>();
    }

    public void SetAnswer(Answer answer)
    {
        Image.sprite = answer.Sprite;
        Answer = answer.Name.ToUpper();
        CreateSlots();
    }
}
