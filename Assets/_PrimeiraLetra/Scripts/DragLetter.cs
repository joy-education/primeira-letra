﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class DragLetter : DraggableElement {

    [System.Serializable]
    public class LetterCollision : UnityEvent<DragLetter, Collider2D> { }

    public LetterCollision OnTriggerEnterEvent = new LetterCollision();
    public LetterCollision OnTriggerExitEvent = new LetterCollision();

    private Text _text;
    public Text Text
    {
        get
        {
            if (_text == null)
                _text = GetComponentInChildren<Text>();
            return _text;
        }
    }

    public string Value
    {
        get
        {
            return Text.text;
        }
        set
        {
            Text.text = value;
        }
    }

    private List<LetterSlot> _collidingWith = new List<LetterSlot>();

    void Awake()
    {
        OnEnd.AddListener((e) => 
        {
            if (_collidingWith.Count > 0 && GameManager.Instance.CheckMatch(this, GetNearestCollidingSlot()))
            {
                Debug.Log("yay");
            }
            else
            {
                Debug.Log("Aww");
            }

            ReturnToStartPosition();
        });
    }

    public bool Colliding { get; set; }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        LetterSlot slot = collision.GetComponent<LetterSlot>();

        if (slot != null)
            _collidingWith.Add(slot);

        OnTriggerEnterEvent.Invoke(this, collision);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        LetterSlot slot = collision.GetComponent<LetterSlot>();

        if (slot != null)
            _collidingWith.Remove(slot);

        OnTriggerExitEvent.Invoke(this, collision);
    }

    private LetterSlot GetNearestCollidingSlot()
    {
        float nearestDistance = Mathf.Infinity;
        LetterSlot nearestSlot = null;

        for (int i = 0; i < _collidingWith.Count; i++)
        {
            float distance = Vector2.Distance(transform.position, _collidingWith[i].transform.position);
            if (distance < nearestDistance)
            {
                nearestSlot = _collidingWith[i];
                nearestDistance = distance;
            }
        }

        return nearestSlot;
    }

    public void ReturnToStartPosition()
    {
        transform.position = StartPosition;
    }

}
